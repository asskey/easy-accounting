<div class="container-fluid">
  <div class="row">
    <div class="col-sm-3 col-md-2 sidebar">
      <ul class="nav nav-sidebar">
        <li class="active"><a href="#">Overview <span class="sr-only">(current)</span></a></li>
        <li><a href="#">Reports</a></li>
        <li><a href="#">Analytics</a></li>
        <li><a href="#">Export</a></li>
      </ul>
      <ul class="nav nav-sidebar">
        <li><a href="">Nav item</a></li>
        <li><a href="">Nav item again</a></li>
        <li><a href="">One more nav</a></li>
        <li><a href="">Another nav item</a></li>
        <li><a href="">More navigation</a></li>
      </ul>
      <ul class="nav nav-sidebar">
        <li><a href="">Nav item again</a></li>
        <li><a href="">One more nav</a></li>
        <li><a href="">Another nav item</a></li>
      </ul>
    </div>
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
      <h1 class="page-header">Dashboard</h1>

      <div class="row placeholders">
        <div class="col-md-12 placeholder">
            <form class="form-horizontal">
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Label</label>
                    <div class="col-sm-10">
                        <input type="email" class="form-control" id="inputEmail3" placeholder="Email">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputSerial3" class="col-sm-2 control-label">Serial</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputSerial3" placeholder="Serial">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputUnitCost3" class="col-sm-2 control-label">Unit Cost</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="inputUnitCost3" placeholder="Unit Cost">
                    </div>
                    <div class="col-sm-1">
                        <input type="text" class="form-control" id="inputUnitCost3" placeholder="Unit Cost">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputUnitSold3" class="col-sm-2 control-label">Unit Sold</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="inputUnitSold3" placeholder="Unit Sold">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox"> Remember me
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default">Sign in</button>
                    </div>
                </div>
            </form>
        </div>

      </div
    </div>
  </div>
</div>
