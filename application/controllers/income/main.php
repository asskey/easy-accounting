<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	/**
	 * Income index
	 */
	public function index() {
		echo "index";
	}

	public function add() {
		$this->load->view('header');
		$this->load->view('income/add');
		$this->load->view('footer');
	}
}
