<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	/**
	 * Index Page for this controller.
	 */
	public function index() {
        echo "welcome to Dashboard";
        $this->load->view("header");
        $this->load->view("dashboard/index");
        $this->load->view("footer");
	}

	public function add() {
		$this->load->view('header');
		$this->load->view('income/add');
		$this->load->view('footer');
	}
}
